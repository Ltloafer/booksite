<?php

namespace LtLoafer\bookSite\Facades;

use Illuminate\Support\Facades\Facade;

class bookSite extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'booksite';
    }
}
