<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\User;
use App\Portfolio;
use App\SharedPortfolio;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\CustomPropertyText;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;

class ClonePortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $originalPortfolio = Portfolio::find($request->input('portfolioId'));

        $portfolio = new Portfolio();
        $portfolio->name = $request->input('clone_name');
        $portfolio->type = 'custom';
        $portfolio->password = $request->input('custom_password');
        $randomString = Str::random(8);
        $portfolio->identifier = $randomString;
        $portfolio->save(); 

        $portfolio->setSluggedName();

        foreach($originalPortfolio->properties as $property) {

            $portfolioProperty = $portfolio->properties()->create([
                'property_id' => $property->property_id,
                'position_no' => $property->position_no
            ]);

            $customText = CustomPropertyText::where('portfolio_id', $originalPortfolio->id)
            ->where('property_id', $property->property_id)
            ->first();

            if ($customText) {
                $text = new CustomPropertyText();
                $text->text = $customText->text;
                $text->portfolio_id = $portfolio->id;
                $text->property_id = $property->property_id;
                $text->save();
            }
        }

        return redirect('/book-site/admin/custom-portfolios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sluggedName)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
