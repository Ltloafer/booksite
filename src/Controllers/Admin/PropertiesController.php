<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use File;
use Storage;
use App\Property;
use CloudFrontUrlSigner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use Illuminate\Support\Facades\Storage as FileStorage;

use Illuminate\Support\Str;


class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $properties = Property::all();

       foreach($properties as $property) {
        $property->fullAddress  = $property->displayFullAddress();
    }

    return view('bookSite::admin.properties')->with('properties', $properties);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property();

        $property->address = 'address';
        $property->house_no = $request->input('house_no');
        $property->street_name = $request->input('street_name');
        $property->town = $request->input('town');
        $property->postcode = $request->input('postcode');

        if ($request->input('contact_status') == 'direct') {
            $property->direct = true;
        }

        $property->save();

        $property->slugged_name = $property->formSluggedAddress();

        $property->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $property = Property::where('slugged_name', $sluggedName)->first();

        if (! $property) {
            abort(403);
        }

        $directory = storage_path(). '/app/public/images/'. $property->id;

         if (File::isDirectory($directory)) {
            $imageFiles = File::allFiles($directory); 
        } else {
            $imageFiles = collect();
        }

        return view('bookSite::admin.property')->with('property', $property)->with('imageFiles' , $imageFiles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $property = Property::find($id);

        $property->house_no = $request->input('house_no');
        $property->street_name = $request->input('street_name');
        $property->town = $request->input('town');
        $property->postcode = $request->input('postcode');

        $property->video_link_url = $request->input('video_link_url');

        if ($request->input('contact_status') == 'direct') {
            $property->direct = true;
        } else {
            $property->direct = null;
        }

        $property->save();

        $property->slugged_name = $property->formSluggedAddress();

        $property->save();

        return $this->show($property->slugged_name);

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::find($id);

        $property->delete();

        return $this->index();
    }

}
