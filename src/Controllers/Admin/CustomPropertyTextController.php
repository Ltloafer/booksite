<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\Property;
use App\Portfolio;
use Illuminate\Http\Request;
use App\CustomPropertyText;
use App\Http\Controllers\Controller;

class CustomPropertyTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Property $property)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $propertyId)
    {
        $property = Property::findOrFail($propertyId);

        $portfolio = Portfolio::findOrFail($request->input('portfolioId'));

        $customText = CustomPropertyText::where('portfolio_id', $portfolio->id)
        ->where('property_id', $property->id)
        ->first();


        if ($customText) {
            if ($request->input('text')) {
                $customText->text = $request->input('text');
                $customText->save();
            } else {
                $customText->delete();
            }
            return back();
        }

        $text = new CustomPropertyText();
        $text->text = $request->input('text');
        $text->portfolio_id = $portfolio->id;
        $text->property_id = $property->id;
        $text->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
