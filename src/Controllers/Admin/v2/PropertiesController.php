<?php

namespace LtLoafer\bookSite\Controllers\Admin\v2;

use Storage;
use App\Property;
use CloudFrontUrlSigner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;


class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $properties = Property::all();

       foreach($properties as $property) {
        $property->fullAddress  = $property->displayFullAddress();
    }

    return view('bookSite::admin.properties')->with('properties', $properties);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property();

        $property->address = 'address';
        $property->house_no = $request->input('house_no');
        $property->street_name = $request->input('street_name');
        $property->town = $request->input('town');
        $property->postcode = $request->input('postcode');

        if ($request->input('contact_status') == 'direct') {
            $property->direct = true;
        }

        $property->save();

        $property->slugged_name = $property->formSluggedAddress();

        $property->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $property = Property::where('slugged_name', $sluggedName)->first();

        if (! $property) {
            abort(403);
        }

        return view('bookSite::admin.property')->with('property', $property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $property = Property::find($id);

        $property->house_no = $request->input('house_no');
        $property->street_name = $request->input('street_name');
        $property->town = $request->input('town');
        $property->postcode = $request->input('postcode');

        if ($request->input('contact_status') == 'direct') {
            $property->direct = true;
        } else {
            $property->direct = null;
        }

        $property->save();

        $property->slugged_name = $property->formSluggedAddress();

        $property->save();

        return $this->show($property->slugged_name);

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::find($id);

        $property->delete();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview($sluggedName)
    {
        if (! $property = Property::where('slugged_name', $sluggedName)->first()) {
            abort(403);
        }

        $images = $property->images->sortBy('position_no');

        $count = 1;

        foreach ($images as $image) {
            $image->propertyId = $image->property_id;
            $image->no = $count;

            if ($image->aws_filepath) {
               
                // $image->url = Storage::disk('s3')->temporaryUrl('properties/' . $property->id . '/' . $image->filename, now()->addMinutes(10));
                $image->url = CloudFrontUrlSigner::sign(config('cloudfront-url-signer.cloudfront_url') . '/properties/' . $property->id . '/' . $image->filename);
                // $image->url = CloudFrontUrlSigner::sign('https://d1i1vr8kbuxg60.cloudfront.net/properties/' . $property->id . '/' . $image->filename, 10);
                // 
                \Log::info($image->url);

            } else {

                  $image->url = '/storage/images/' .  $image->propertyId . '/' . $image->filename;
            }

            $count +=1;
        }

        $total = $images->count();

        return view('bookSite::properties.v2.property')->with(compact('property', 'images', 'total'));
    }

    // public function setPropertyDetails($reset = null)
    // {
    //     if ($reset) {
    //         $properties = Property::all();
    //     } else {
    //         $properties = Property::whereNull('slugged_name')->get();
    //     }


    //     foreach ($properties as $property) {

    //         $sluggedName = Str::slug($property->slugAddress(), '-');

    //         $existingSluggedName = Property::where('slugged_name', $sluggedName)->first();


    //         if ($existingSluggedName) {

    //             $counter = 1;

    //             while ($existingSluggedName) {

    //                 $newPropertyName = $property->slugAddress(). '_' .$counter;

    //                 $newSluggedName = Str::slug($newPropertyName, '-');

    //                 $existingSluggedName = Property::where('slugged_name', $newSluggedName)->first();

    //                 $counter += 1;
    //             }

    //             $sluggedName = $newSluggedName;
    //         }

    //         $property->slugged_name = $sluggedName;

    //         $property->save();
    //     }

    //     dd('Complete');
    // }

}
