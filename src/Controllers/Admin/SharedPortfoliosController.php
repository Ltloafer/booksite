<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\User;
use App\Portfolio;
use App\SharedPortfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LtLoafer\bookSite\Mail\SharePortfolio;

use Illuminate\Support\Str;

class SharedPortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sharedPortfolio = new SharedPortfolio();

        $sharedPortfolio->name = $request->input('client_name');
        $sharedPortfolio->email = $request->input('client_email');
        $sharedPortfolio->portfolio_id = $request->input('portfolioId');
        $sharedPortfolio->status = 'live';

        $sharedPortfolio->requestId = Str::random(20);

        $sharedPortfolio->save();
        
         // return new SharePortfolio($sharedPortfolio);

        \Mail::to($sharedPortfolio->email)->send(new SharePortfolio($sharedPortfolio));


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sluggedName)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
