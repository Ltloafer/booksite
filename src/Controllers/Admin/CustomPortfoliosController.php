<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\User;
use App\Property;
use App\Portfolio;
use App\PortfolioProperty;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CustomPortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::where('type', 'custom')->get()->sortBy('name')->values();

        return view('bookSite::admin.custom-portfolios')->with('portfolios', $portfolios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolio = new Portfolio();

        $portfolio->name = $request->input('custom_name');
        $portfolio->type = 'custom';
        $portfolio->password = $request->input('custom_password');
        
        $randomString = Str::random(8);
        $portfolio->identifier = $randomString;

        $portfolio->save();

        $portfolio->setSluggedName();

        return $this->show($portfolio->slugged_name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        if (! $portfolio) {
            return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $portfolioProperties = $portfolio->properties;

        if ($portfolioProperties) {

            $properties = collect();

            foreach($portfolioProperties as $portfolioProperty) {

                if ($property = Property::find($portfolioProperty->property_id)) {
                    $property->position_no = $portfolioProperty->position_no;
                    $property->customText = $property->getCustomText($portfolio->id);
                    $properties->push($property);
                }
            }
        } else {
            $properties = collect();
        }

        $properties = $properties->sortBy('position_no');

        return view('bookSite::admin.custom-portfolio')->with('portfolio', $portfolio)->with('properties', $properties);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sluggedName)
    {
        // $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        // if (! $portfolio) {
        //     return back()->with('flash_message', 'This portfolio does not exist.');
        // }

        // $properties = Property::all();

        // $portfolioPropertyIds = PortfolioProperty::where('portfolio_id', $portfolio->id)->pluck('property_id');

        // $portfolioProperties = Property::whereIn('id', $portfolioPropertyIds)->get();

        // $properties = Property::whereNotIn('id', $portfolioPropertyIds)->get();

        // return view('bookSite::admin.portfolio-edit')->with('portfolio', $portfolio)->with('properties', $properties)->with('portfolioProperties', $portfolioProperties);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        
        $portfolio->delete();

        return $this->index();
    }

    public function setIdentifier(Request $request)
    {
        $portfolio = Portfolio::find($request->input('portfolioId'));
        
        $portfolio->password = $request->input('custom_password');
        
        $randomString = Str::random(8);
        $portfolio->identifier = $randomString;

        $portfolio->save();

        return back();
    }

     public function setPassword(Request $request)
    {
        $portfolio = Portfolio::find($request->input('portfolioId'));
        
        $portfolio->password = $request->input('custom_password');
        
        $randomString = Str::random(8);
        $portfolio->identifier = $randomString;

        $portfolio->save();

        return back();
    }


}
