<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\User;
use App\Property;
use App\PropertyImage;
use App\Portfolio;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\PopulatePortfolioService;

use Illuminate\Support\Str;

class PortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();

        $users = User::all();

        return view('admin.portfolios')->with('portfolios', $portfolios)->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolio = new Portfolio();

        $portfolio->user_id = $request->input('userId');
        $portfolio->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        if (! $portfolio) {
            return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $portfolioProperties = $portfolio->properties;

        $properties = collect();
        $directProperties = collect();

        foreach($portfolioProperties as $portfolioProperty) {

            if ($property = Property::find($portfolioProperty->property_id)) {
                $property->position_no = $portfolioProperty->position_no;
                $property->customText = $property->getCustomText($portfolio->id);
                $properties->push($property);
            }
        }

        $properties = $properties->sortBy('position_no');
        // $directProperties = $directProperties->sortBy('position_no');

        return view('bookSite::admin.portfolio')->with('portfolio', $portfolio)
        // ->with('directProperties', $directProperties)
        ->with('properties', $properties);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sluggedName)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        if (! $portfolio) {
            return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $portfolioPropertyIds = PortfolioProperty::where('portfolio_id', $portfolio->id)->pluck('property_id');

        $portfolioProperties = Property::whereIn('id', $portfolioPropertyIds)->get();

        foreach($portfolioProperties as $property) {
            $property->displayFullAddress = $property->displayFullAddress();
        }

        $properties = Property::whereNotIn('id', $portfolioPropertyIds)->get();

        $properties = $properties->sortBy('street_name');

        foreach ($properties as $property) {

         if ($property->title_image_id) {

            try {

                $filename = PropertyImage::find($property->title_image_id)->filename;

            } catch (\Exception $e) {

                $filename = $property->images->first()->filename;
            }


        } else {

            $filename = $property->images->first()->filename ?? '';
        }

        $property->displayFullAddress = $property->displayFullAddress();

        $property->imageFilename = '/storage/images/'. $property->id . '/mobile/'. $filename;
    }

    return view('bookSite::admin.portfolio-edit')->with('portfolio', $portfolio)->with('properties', $properties->values())->with('portfolioProperties', $portfolioProperties);
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        //
    }

    public function setPortfolioDetails()
    {
        $portfolios = Portfolio::whereNull('name')->get();

        foreach ($portfolios as $portfolio) {

            $portfolio->name = $portfolio->user->name. ' ' .$portfolio->user->last_name;

            $sluggedName = Str::slug($portfolio->name, '-');

            $existingPortfolioName = Portfolio::where('slugged_name', $sluggedName)->first();


            if ($existingPortfolioName) {

                $counter = 1;

                while ($existingPortfolioName) {

                    $newPortfolioName = $portfolio->name. '_' .$counter;

                    $newSluggedName = Str::slug($newPortfolioName, '-');

                    $existingPortfolioName = Portfolio::where('slugged_name', $newSluggedName)->first();

                    $counter += 1;
                }

                $sluggedName = $newSluggedName;
            }

            $portfolio->slugged_name = $sluggedName;

            $portfolio->save();
        }

        dd('Complete');
    }


}
