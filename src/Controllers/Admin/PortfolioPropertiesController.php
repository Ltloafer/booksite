<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\Property;
use App\Portfolio;
use App\PropertyImage;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioPropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    public function getProperties(Portfolio $portfolio)
    {
        $portfolioProperties = $portfolio->properties;

        if ($portfolioProperties) {

            $properties = collect();

            foreach($portfolioProperties as $portfolioProperty) {

                if ($property = Property::find($portfolioProperty->property_id)) {
                    $property->position_no = $portfolioProperty->position_no;
                    $property->customText = $property->getCustomText($portfolio->id);
                    $property->fullAddress = $property->displayFullAddress();
                    $property->hasCustomText = $property->hasCustomText($portfolio->id);
                    $property->price = $portfolioProperty->price;
                    $property->periodText = $portfolioProperty->period_text;
                    $properties->push($property);
                }
            }
        } else {
            $properties = collect();
        }

        $properties = $properties->sortBy('position_no');

        return $properties;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($portfolioId, Request $request)
    {
        $portfolio = Portfolio::find($portfolioId);

        $portfolioProperty = new PortfolioProperty();
        $portfolioProperty->property_id = $request->input('propertyId');
        $portfolioProperty->portfolio_id = $portfolio->id;

        $last = $portfolio->orderedProperties->last();

        if ($last) {
            $portfolioProperty->position_no = $last->position_no + 1;
        } else {
            $portfolioProperty->position_no = 1;
        }

        $portfolioProperty->save();

        return 'success';

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        return view('admin.property')->with('property', $property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $portfolioId, $propertyId)
    {
        $portfolioProperty = PortfolioProperty::where('portfolio_id', $portfolioId)
        ->where('property_id', $propertyId)
        ->first();

        if ($portfolioProperty) {

            $portfolioProperty->price = $request->input('price');

            $portfolioProperty->period_text = $request->input('period');

            $portfolioProperty->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio, Property $property)
    {
        $portfolioProperty = PortfolioProperty::where('portfolio_id', $portfolio->id)
        ->where('property_id', $property->id)
        ->first();

        if ($portfolioProperty) {
            $portfolioProperty->delete();
        }

        return back();
    }

    public function setPortfolioPropertyPositioning()
    {
        // dd('set positioning');

        $portfolios = Portfolio::all();

        foreach ($portfolios as $portfolio) {

            $positionNo = 1;

            foreach ($portfolio->properties as $property) {
                $property->position_no = $positionNo;
                $property->save();
                $positionNo = $positionNo + 1;
            }
        }
    }

    public function positionProperties(Request $request, Portfolio $portfolio)
    {
        $position = 1;

        foreach ($request->properties as $property) {

            $portfolioProperty = PortfolioProperty::where('portfolio_id', $portfolio->id)
            ->where('property_id', $property['id'])->first();

            $portfolioProperty->position_no = $position;
            $portfolioProperty->save();
            $position +=1;
        }

        return $position;
    }

    public function removeProperty(Request $request)
    {
        $portfolioProperty = PortfolioProperty::where('portfolio_id', $request->input('portfolioId'))
        ->where('property_id', $request->input('propertyId'))
        ->first();

        if ($portfolioProperty) {
            $portfolioProperty->delete();
        }

        return 'success';
    }

    public function getAllPropertiesNotInPortfolio($portfolioId)
    {
        $portfolio = Portfolio::find($portfolioId);

        if (! $portfolio) {
            return 'This portfolio does not exist';
            // return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $portfolioPropertyIds = PortfolioProperty::where('portfolio_id', $portfolio->id)->pluck('property_id');

        // $portfolioProperties = Property::whereIn('id', $portfolioPropertyIds)->get();

        $properties = Property::whereNotIn('id', $portfolioPropertyIds)->get();

        $properties = $properties->sortBy('street_name');

        foreach ($properties as $property) {

         if ($property->title_image_id) {

            try {

                $filename = PropertyImage::find($property->title_image_id)->filename;

            } catch (\Exception $e) {

                $filename = $property->images->first()->filename;
            }


        } else {

            $filename = $property->images->first()->filename ?? '';
        }

        $property->displayFullAddress = $property->displayFullAddress();

        $property->imageFilename = '/storage/images/'. $property->id . '/mobile/'. $filename;
    }

    return $properties->values();
}

}



