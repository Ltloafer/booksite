<?php

namespace LtLoafer\bookSite\Controllers\Preview;

use File;
use Storage;
use App\Property;
use CloudFrontUrlSigner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;


class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $property = Property::where('slugged_name', $sluggedName)->first();

        if (! $property) {
            abort(403);
        }

        $directory = storage_path(). '/app/public/images/'. $property->id;

        if (File::isDirectory($directory)) {
            $imageFiles = File::allFiles($directory); 
        } else {
            $imageFiles = collect();
        }

        return view('bookSite::admin.property')->with('property', $property)->with('imageFiles' , $imageFiles);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview($sluggedName)
    {
        if (! $property = Property::where('slugged_name', $sluggedName)->first()) {
            abort(403);
        }

        $images = $property->images->sortBy('position_no');

        $count = 1;

        foreach ($images as $image) {
            $image->propertyId = $image->property_id;
            $image->no = $count;

            if ($image->aws_filepath) {

                $image->url = Storage::disk('s3')->temporaryUrl('properties/' . $property->id . '/' . $image->filename, now()->addMinutes(10));
                // $image->url = CloudFrontUrlSigner::sign(config('cloudfront-url-signer.cloudfront_url') . '/properties/' . $property->id . '/' . $image->filename, 10);
                // $image->url = CloudFrontUrlSigner::sign('https://d1i1vr8kbuxg60.cloudfront.net/properties/' . $property->id . '/' . $image->filename, 10);
                // 
                \Log::info($image->url);

            } else {

              $image->url = '/storage/images/' .  $image->propertyId . '/' . $image->filename;
          }

          $count +=1;
      }

      $total = $images->count();

      return view('bookSite::preview.property')->with(compact('property', 'images', 'total'));
  }


}
