<?php

namespace LtLoafer\bookSite\Controllers\Preview;

use App\User;
use App\Property;
use App\PropertyImage;
use App\Portfolio;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\PopulatePortfolioService;

use Illuminate\Support\Str;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        if (! $portfolio) {
            return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $portfolioProperties = $portfolio->properties;

        $properties = collect();
        $directProperties = collect();

        foreach($portfolioProperties as $portfolioProperty) {

            if ($property = Property::find($portfolioProperty->property_id)) {
                $property->position_no = $portfolioProperty->position_no;
                $property->customText = $property->getCustomText($portfolio->id);
                $properties->push($property);
            }
        }

        $properties = $properties->sortBy('position_no');

        return view('bookSite::admin.portfolio')->with('portfolio', $portfolio)
        ->with('properties', $properties);
    }


    public function preview($sluggedName, PopulatePortfolioService $populatePortfolio)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        if (! $portfolio) {
            return back()->with('flash_message', 'This portfolio does not exist.');
        }

        $data = $populatePortfolio->populatePortfolio($portfolio);

        return view('bookSite::preview.portfolio')->with('properties', $data['properties'])->with('portfolio', $data['portfolio']);

    }


}
