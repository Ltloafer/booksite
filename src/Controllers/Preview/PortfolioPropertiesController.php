<?php

namespace LtLoafer\bookSite\Controllers\Preview;

use App\Property;
use App\Portfolio;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioPropertiesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sluggedName, $sluggedProperty)
    {
        $portfolio = Portfolio::where('slugged_name', $sluggedName)->first();

        $property = Property::where('slugged_name', $sluggedProperty)->first();

        if (! $property) {
            abort(403);
        }

        // if ($portfolio->shouldNotBeViewed()) {
        //     abort(403);
        // }

        if ($portfolio->doesntContain($property)) {
            abort(403);
        }

        $images = $property->images->sortBy('position_no');

        $count = 1;

        foreach ($images as $image) {
            $image->propertyId = $image->property_id;
            $image->no = $count;
            $count +=1;
        }

        $total = $images->count();

        return view('bookSite::preview.portfolios.property')->with(compact('property', 'images', 'total', 'portfolio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
