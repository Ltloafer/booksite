<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your this package. These
| routes are loaded by the package ServiceProvider. 
*/

Route::group(['prefix' => 'book-site/admin', 'middleware' => ['admin']], function () {

	Route::get('/getProperty/{property}', 'Admin\PropertyImagesController@getProperty');
	Route::get('/getPropertyImages/{property}', 'Admin\PropertyImagesController@getImagesForProperty');
	Route::post('/setHeaderImage/{image}', 'Admin\PropertyImagesController@setHeaderImage');
	Route::post('/removeImage/{image}', 'Admin\PropertyImagesController@removeImage');
	Route::post('/positionPropertyImages/{property}', 'Admin\PropertyImagesController@positionImages');

	Route::post('/positionPortfolioProperties/{portfolio}', 'Admin\PortfolioPropertiesController@positionProperties');

	// Route::get('/preview-property/{property}', 'Admin\PropertiesController@preview');

	Route::get('/getPortfolioProperties/{portfolio}', 'Admin\PortfolioPropertiesController@getProperties');

	Route::get('/getAllPropertiesNotInPortfolio/{portfolio}', 'Admin\PortfolioPropertiesController@getAllPropertiesNotInPortfolio');

	// Route::get('/setPortfolioPropertyPositioning/{portfolio}', 'Admin\PortfolioPropertiesController@setPortfolioPropertyPositioning');
	// 
	Route::get('/setPortfolioDetails', 'Admin\PortfoliosController@setPortfolioDetails');

	Route::get('/setPropertyDetails/{reset?}', 'Admin\PropertiesController@setPropertyDetails');

	Route::resource('/dashboard', 'Admin\DashboardController');
	Route::resource('/users', 'Admin\UsersController');
	Route::resource('/properties', 'Admin\PropertiesController');

	Route::resource('/images', 'Admin\ImagesController');

	Route::post('/set-portfolio-identifier', 'Admin\CustomPortfoliosController@setIdentifier');
	Route::post('/set-portfolio-password', 'Admin\CustomPortfoliosController@setPassword');

	Route::resource('/portfolios', 'Admin\PortfoliosController');

	Route::resource('/custom-portfolios', 'Admin\CustomPortfoliosController');
	Route::resource('/shared-portfolio', 'Admin\SharedPortfoliosController');

	Route::resource('/clone-portfolio', 'Admin\ClonePortfoliosController');

	Route::post('removePropertyFromPortfolio', 'Admin\PortfolioPropertiesController@removeProperty');
	Route::resource('/portfolio.properties', 'Admin\PortfolioPropertiesController');
	Route::resource('/properties.images', 'Admin\PropertyImagesController');
	Route::resource('/property-text', 'Admin\PropertyTextController');
	Route::resource('/custom-property-texts', 'Admin\CustomPropertyTextController');
});


Route::group(['prefix' => 'book-site/preview', 'middleware' => ['admin']], function () {
	
	Route::get('/property/{property}', 'Preview\PropertiesController@preview');

	Route::get('portfolios/{slugged_name}', 'Preview\PortfolioController@preview');
	Route::resource('portfolio.properties', 'Preview\PortfolioPropertiesController');
});





