<?php

namespace LtLoafer\bookSite\Mail;

use App\SharedPortfolio;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SharePortfolio extends Mailable
{
    use Queueable, SerializesModels;

    public $sharedPortfolio;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SharedPortfolio $sharedPortfolio)
    {
        $this->sharedPortfolio = $sharedPortfolio;
        // $this->url = url('viewPortfolio/'.$sharedPortfolio->requestId);
        $this->url = url('portfolios/'.$sharedPortfolio->portfolio_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name')
         return $this->from('neilbaker26@hotmail.com')
        ->subject('Shared portfolio')
        ->markdown('bookSite::emails.sharedPortfolio');
        // ->view('emails.passportRequest_html');
    }
}
