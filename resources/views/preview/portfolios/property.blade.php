@extends('layouts.app')
@section('content')

<property :images = "{{ $images }}" :total= "{{ $total }}" :property=" {{ $property }}" inline-template>

    <div class="container" style="border-style: none">
        <div class="close-property">
              <a href="/book-site/preview/portfolios/{{ $portfolio->slugged_name }}"><img src="{{url('/images/cross-out.png')}}"></a>
        </div>

        <!-- <i class="fas fa-times pull-right"></i></a> -->
        <div class="row justify-content-center full-height">
            <div class="col-12 align-self-centre" style="padding:10px" v-cloak>

                <div class="nplh">
                    <div class="middle-list" >
                        <div class="properties-golden-ticket" style="border-bottom: 1px solid currentColor"><strong>GT</strong></div>
                        <div class="properties-london"><strong> L</strong></div>
                    </div>
                </div>

                <agile :options="myOptions"
                :unagile="false"
                :speed="1000" 
                :timing="'linear'" 
                :fade="true" 
                :autoplay="false"
                :centre-padding="true"
                :dots="true"
                :mobile-first="true"
                @afterChange="showCurrentSlide($event)">

                <template v-if="property.description">
                    <div class="slide" v-show="slideVisible">
                        <div class="description-text">
                            <p>{{ $property->description }}</p>
                        </div>
                        <div class="custom-text mt-4">
                            <p>{{ $property->getCustomText($portfolio->id) }}</p>
                        </div>
                    </div>
                </template>


                <template v-for="image in orderedImages">
                    <div class="slide" v-show="slideVisible">
                        <img :src=" '/storage/images/' + image.propertyId + '/' + image.filename">

                        <div class="bottom-counter">
                            <span style="border-bottom: 1px solid currentColor">@{{ image.no }}</span>
                            <div>@{{ total }}</div>
                        </div>
                    </div>
                </template>
                

                <template v-if="property.show_google_map">
                    <div class="slide" v-show="slideVisible">
                        <gmap-map
                        :center="{lat: latitude, lng: longitude}"
                        :zoom="17"
                        map-type-id="roadmap"
                        :options="{
                        zoomControl: false,
                        mapTypeControl: false,
                        scaleControl: true,
                        streetViewControl: false,
                        rotateControl: false,
                        fullscreenControl: false,
                        disableDefaultUi: false,
                        scrollWheelZoom: false,
                        marker: true
                    }">

                    <gmap-marker
                    :key="index"
                    v-for="(m, index) in markers"
                    :position="m.position"
                    :clickable="false"
                    :draggable="false"
                    @click="center=m.position"
                    />
                </gmap-map>
            </div>
        </template>

        <template slot="prevButton"><img src="{{url('/images/back.png')}}"></template>
        <template slot="nextButton"><img src="{{url('/images/next.png')}}"> </template>
    </agile>

    <div class="close-property-mobile v-cloak">
        <div class="middle-list mobile-counter" v-cloak>
            <div class="mobile-image-counter">@{{ imageNo }}</div>
            <div v-if="imageNo">@{{ getTotal() }}</div>
        </div>
    </div>
</div>
</div>
</div>

</property>
@endsection
