<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="icon" href="../../../../favicon.ico"> -->

  <title>GT Portfolio</title>

  <!-- Bootstrap core CSS -->
  <!-- <link href="../../../../dist/css/bootstrap.min.css" rel="stylesheet"> -->

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Golden Ticket') }}</title>

  <!-- Scripts -->
  <script src="{{ mix('/js/app.js') }}" defer></script>
  <script src="{{ asset('js/holder.min.js') }}" defer></script>

  <!-- Custom styles for this template -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/p.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('css/blog.css') }}" rel="stylesheet"> -->

  <link href="https://fonts.googleapis.com/css?family=Rock+Salt&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <style>

    html {
      scroll-behavior: smooth;
    }

    .fade-enter-active, .fade-leave-active {
      transition: opacity .5s;
    }
    .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
      opacity: 0;
    }



    body {
      margin: 0;
      font-family: "Nunito", sans-serif;
      font-size: 0.8rem;
      font-weight: 400;
      line-height: 1.6;
      color: #212529;
      text-align: left;
      /*background-color: #f2eee8;*/
      background-color: #fff;
      /*padding-top: 100px;*/
    }

    a:hover {
      text-decoration: none;
    }


    .hovereffect {
      width:100%;
      height:100%;
      float:left;
      overflow:hidden;
      position:relative;
      text-align:center;
      /*cursor:default;*/
      cursor:pointer;
    }

    .hovereffect .overlay-two {
      width:100%;
      height:100%;
      position:absolute;
      overflow:hidden;
      top:0;
      left:0;

      opacity:1;
      /*background-color:rgba(0,0,0,0.5);*/
      -webkit-transition:all .4s ease-in-out;
      transition:all .4s ease-in-out
    }

    .hovereffect img {
      display:block;
      position:relative;
      -webkit-transition:all .4s linear;
      transition:all .4s linear;
    }

    .hovereffect h2 {
      text-transform:uppercase;
      color:black;
      text-align:left;
      position:relative;
      font-size:12px;
      /*background:rgba(0,0,0,0.1);*/
      background-color: hsla(0, 0%, 100%, .45);
/*    -webkit-transform:translatey(-100px);
    -ms-transform:translatey(-100px);
    transform:translatey(-100px);
    -webkit-transition:all .2s ease-in-out;
    transition:all .2s ease-in-out;*/
    padding:7px;
    padding-left: 10px;
  }

  footer {
  /* position: fixed;
   bottom: 0;
   left: 0;
   width: 100%;*/
   padding-top: 2rem;
   padding-bottom: 2rem;
 }

 .blur-up {
  -webkit-filter: blur(5px);
  filter: blur(5px);
  transition: filter 400ms, -webkit-filter 400ms;
}

.blur-up.lazyloaded {
  -webkit-filter: blur(0);
  filter: blur(0);
}


.blurry-up {
  /*-webkit-filter: blur(5px);*/
  /*filter: blur(5px);*/
  opacity: 0;
  transition: opacity 400ms, -webkit-filter 400ms;
}

.fade-box .lazyload,
.fade-box .lazyloading {
  opacity: 0;
  transition: opacity 400ms;
}

.fade-box img.lazyloaded {
  opacity: 1;
}

.nav-border {
  border-bottom: 1px solid rgb(127, 82, 23, 0.5);
}


</style>

</head>

<body>
  <div id="app">
    <div class="container-fluid" style="padding-left: 0px; padding-right: 0px">
      <header>
        <nav class="navbar navbar-light bg-white d-none d-sm-block nav-border">
          <div class="container d-flex justify-content-between">
            <div class="col-4 pt-1 d-flex justify-content-between">
              <!-- <a class="btn btn-sm btn-outline-secondary" href="/menu">Main menu</a> -->

            </div>

            <div class="col-4 text-center d-none d-sm-block">
              <a href="/book-site/admin/custom-portfolios">
                <h3 style="color:#805649; line-height: 1.3; letter-spacing: 8px; font-weight: 600; transform: scaleY(0.8);">GOLDEN TICKET</h3>
              </a>
              <!-- <img id="title_image"  src="../images/gt_london_ticket.svg" height="75px" width="auto"> -->

            </div>

            <div class="col-4 d-flex justify-content-end align-items-center">

              <!-- <small class="text-muted mr-3"><i class="far fa-heart fa-2x"></i></small> -->


                  <!--   <a class="text-muted" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mx-3"><circle cx="10.5" cy="10.5" r="7.5"></circle><line x1="21" y1="21" x2="15.8" y2="15.8"></line></svg>
                      </a> -->
                 <!--      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation" style="border: none">
                        <span class="navbar-toggler-icon"></span>
                      </button> 
                    -->
                  </div>
                </div>
              </nav>
            </header>

          </div>


          <main role="main">

           @yield('content')

         </main>

       </div>




        <!--    <footer class="text-muted bg-dark">

            <div class="container">

              <p class="float-right"> -->
                <!-- <a href="#">Back to top</a> -->
           <!--    </p>

              <p>Golden Ticket London</p>
              <p>71 - 75 Shelton Street, London WC2H 9JQ</p>
              <p>Telephone + 44 203 858 7383</p>
              <p>Built by OnOrigin Ltd</p>

            </div>

          </footer>
        -->


    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>

      <script>
        window.onpageshow = function(event) {
          if (event.persisted) {
           window.location.reload() ;
         }
       };

     </script>
     <!-- <script src="../../public/js/holder.min.js"></script> -->
     <!-- <script src="../../../../dist/js/bootstrap.min.js"></script> -->
     <!-- <script src="../../../../assets/js/vendor/holder.min.js"></script> -->
     <!-- <script src="{{ mix('/js/app.js') }}" defer></script> -->
     <!-- <link href="{{ asset('js/holder.min.js') }}"> -->


   </body>
   </html>

