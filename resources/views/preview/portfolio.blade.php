@extends('bookSite::preview.layouts.portfolios')
@section('content')

<div class="album py-5">
    <div class="container d-flex justify-content-center pb-3">
        <h6 style="
        font-weight: 600;
        letter-spacing: 2px;
        color: rgb(127, 82, 23);
        text-transform: uppercase;"
        >
        {{ $portfolio->name }}</h6>
    </div>


    <portfolio :properties= "{{ $properties }}" :portfolio="{{ $portfolio}}" inline-template>

        <div>

            <div class="container-fluid" v-cloak>

                <div class="row p-3">

                    <template v-for="(property,key) in propos">

                        <div class="col-md-4 pb-4">

                            <div class="card mb-4 rounded-0"> 

                                <a v-bind:href=" '/book-site/preview/portfolio/' + portfolio.slugged_name + '/properties/' + property.slugged_name " 
                                @mouseover="hovered = key" @mouseleave="hovered = null">

                                <div class="hovereffect">

                                    <transition mode="out-in" name="fade">

                                        <template v-if="hovered === key">

                                            <img class="card-img-top" 
                                            :src="property.propertySecondImage" 
                                            height="230px" width="162px"  style="object-fit: cover" alt="image">

                                        </template>

                                        <template v-else>

                                            <img class="card-img-top lazyload blur-up" 
                                            :src="property.titleImageMedium" 
                                            height="230px" width="162px"  style="object-fit: cover" alt="image">

                                        </template>

                                    </transition>


                                <!--     <div class="overlay-two">
                                        <h2>hello</h2>
                                    </div> -->

                                </div>

                            </div>

                        </a>

                        <div class="mb-4" style="font-size: 97%; text-transform: uppercase; letter-spacing: 0.03rem">

                            <strong>@{{ property.street_name }}</strong> <span style="float: right">@{{ property.price }}</span><br>
                            <strong>@{{ property.town }} @{{ property.postcode }}</strong><span style="float: right">@{{ property.period }}</span><br>

                        </div>

                    </div>

                </template>

            </div>

        </div>

    </div>

</portfolio>

</div>

@endsection

