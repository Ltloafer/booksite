@extends('bookSite::properties.v2.layout')

@section('content')

<!-- Begin Content -->
<div class="main-container with-padding">

    <property :images = "{{ $images }}" :total= "{{ $total }}" :property=" {{ $property }}" inline-template>

        <!-- Begin Wrapper -->
        <div class="wrapper">

            <!-- Begin Portfolio -->
            <div class="portfolio-single style-1">

                <!-- Begin Navigation -->
                <div class="nav">
                    <a href="#" class="prev icon4-leftarrow23"></a>
                    <a href="#" class="next icon4-chevrons"></a>
                </div>
                <!-- End Navigation -->

                <!-- Begin Inner Wrapper -->
                <div class="row inner-wrapper">

                    <div class="col full content clearfix">

                        <div class="info">
                            <div class="inner">

                                <div class="info-box">
                                    <p class="desc">PROPERTY</p>
                                    <p class="info">13, WESTBOURNE GROVE, W11</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">AREA</p>
                                    <p class="info">PRIMROSE HILL</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">NUMBER OF BEDS</p>
                                    <p class="info">3</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">NUMBER OF BATHS</p>
                                    <p class="info">3</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">OUTSIDE SPACE</p>
                                    <p class="info">YES</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">OFF-STREET PARKING</p>
                                    <p class="info">YES</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">LOCAL GEMS</p>
                                    <p class="info">SWIMMING BATHS</p>
                                    <p class="info">BARS</p>
                                    <p class="info">RESTUARANTS</p>
                                </div>

                             <!--    <div class="info-box">
                                    <p class="desc"></p>
                                    <p class="info">
                                        <a href="#">FACEBOOK</a><br/>
                                        <a href="#">TWITTER</a><br/>
                                        <a href="#">GOOGLE+</a><br/>
                                        <a href="#">PINTEREST</a>
                                    </p>
                                </div>
                            -->
                        </div>
                    </div>

                    <div class="images">

                        @foreach($images as $image)

                        <a href="{{ $image->url }}" rel="gallery">
                            <img class="lazy" data-original="{{ $image->url }}" alt="" data-width="850" data-height="785">
                        </a>

                        @endforeach

                        
                    </div>

                </div>

            </div>
            <!-- End Inner Wrapper -->

        </div>
        <!-- End Portfolio -->

    </div>
    <!-- End Wrapper -->

</property>

</div>
<!-- End Content -->

@endsection

