<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<!-- Begin Head -->
<head>
  <meta charset="utf-8">
  <title>Uno Photography</title>

  <!-- Begin Meta Tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="description" content="A Photography Template by CodeSymbol"/>
  <meta name="keywords" content="HTML, CSS, JavaScript, Responsive, Photography"/>
  <meta name="author" content="CodeSymbol"/>
  <!-- End Meta Tags -->


  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Golden Ticket') }}</title>

  <!-- Scripts -->
  <!-- <script src="{{ mix('/js/app.js') }}" defer></script> -->


  <link rel="icon" href="images/favicon.ico" />

  <!-- Begin Stylesheets -->

  <link href="{{ asset('uno/css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/includes/entypo/style.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/includes/icomoon/style.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/includes/font_awesome/font-awesome.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/includes/cosy/style.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/js/jquery-ui/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/js/flexslider/style.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/js/Magnific-Popup/magnific-popup.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/js/mb.YTPlayer/css/YTPlayer.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('uno/css/light.css') }}" rel="stylesheet">

  <!-- End Stylesheets -->


</head>
<!-- End Head -->


<!-- Begin Body -->
<body>

  <!-- Begin Loader -->
  <div class="loader" data-background-color="#000000" data-text-color="#ffffff">
    <p>LOADING</p>
    <span class="circle"></span>
  </div>
  <!-- End Loader -->

  <!-- Begin Header -->
  <header>

    <a href="index.html" class="logo-container">
      <p>GOLDEN TICKET LONDON</p>
    </a>

    <nav>
      <ul>
        <li>
          <a href="#">HOME</a>
          <ul>
            <li>
              <a href="#">
                MEMBERS
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="home_landing_fullscreen_slideshow.html">FULLSCREEN SLIDESHOW</a></li>
                <li><a href="home_landing_striped_slides.html">STRIPED SLIDES</a></li>
                <li><a href="home_landing_striped_pages.html">STRIPED PAGES</a></li>
              </ul>
            </li>
            <li><a href="index.html">HORIZONTAL GALLERY</a></li>
            <li><a href="home_background_image.html">BACKGROUND IMAGE</a></li>
            <li><a href="home_background_video.html">BACKGROUND VIDEO</a></li>
          </ul>
        </li>
        <li>
          <a href="#">
            MEMBERS
          </a>
          <ul>
            <li>
              <a href="#">
                SLIDESHOW
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_slideshow_normal_nav.html">NORMAL NAV</a></li>
                <li><a href="gallery_slideshow_vertical_nav.html">VERTICAL NAV</a></li>
                <li><a href="gallery_slideshow_horizontal_nav.html">HORIZONTAL NAV</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                HORIZONTAL
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_horizontal_normal.html">NORMAL</a></li>
                <li><a href="gallery_horizontal_centered.html">CENTERED</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                ALBUMS
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_albums_grid.html">GRID</a></li>
                <li><a href="gallery_albums_masonry.html">MASONRY</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                GRID
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_grid_icons.html">WITH ICONS</a></li>
                <li><a href="gallery_grid_icons2.html">WITH ICONS 2</a></li>
                <li><a href="gallery_grid_no_icons.html">NO ICONS</a></li>
                <li><a href="gallery_grid_title.html">WITH TITLE</a></li>
                <li><a href="gallery_grid_high_space.html">IMAGES (HIGH SPACE)</a></li>
                <li><a href="gallery_grid_low_space.html">IMAGES (LOW SPACE)</a></li>
                <li><a href="gallery_grid_no_space.html">IMAGES (NO SPACE)</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                MASONRY
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_masonry_icons.html">WITH ICONS</a></li>
                <li><a href="gallery_masonry_icons2.html">WITH ICONS 2</a></li>
                <li><a href="gallery_masonry_no_icons.html">NO ICONS</a></li>
                <li><a href="gallery_masonry_title.html">WITH TITLE</a></li>
                <li><a href="gallery_masonry_high_space.html">IMAGES (HIGH SPACE)</a></li>
                <li><a href="gallery_masonry_low_space.html">IMAGES (LOW SPACE)</a></li>
                <li><a href="gallery_masonry_no_space.html">IMAGES (NO SPACE)</a></li>
              </ul>
            </li>

            <li>
              <a href="#">
                ISOTOPE
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="gallery_isotope_grid.html">GRID</a></li>
                <li><a href="gallery_isotope_masonry.html">MASONRY</a></li>
              </ul>
            </li>
            <li><a href="gallery_kenburns.html">KENBURNS</a></li>
            <li><a href="gallery_vertical.html">VERTICAL</a></li>
          </ul>
        </li>
        <li>
          <!-- <a href="#" class="active"> -->
          <a href="/book-site/admin/custom-portfolios" class="active">
            PORTFOLIOS
          </a>
          <!-- <ul>
            <li><a href="portfolio_grid.html">GRID</a></li>
            <li><a href="portfolio_masonry.html">MASONRY</a></li>
            <li><a href="portfolio_striped.html">STRIPED</a></li>
            <li>
              <a href="#">
                ISOTOPE
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="portfolio_isotope_grid.html">GRID</a></li>
                <li><a href="portfolio_isotope_masonry.html">MASONRY</a></li>
              </ul>
            </li>
            <li>
              <a href="#" class="active">
                SINGLE
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="portfolio_single_1.html" class="active">STYLE 1</a></li>
                <li><a href="portfolio_single_2.html">STYLE 2</a></li>
                <li><a href="portfolio_single_3.html">STYLE 3</a></li>
              </ul>
            </li>
          </ul> -->
        </li>
        <li>
          <a href="/book-site/admin/properties">
            PROPERTIES
          </a>
          <!-- <ul>
            <li><a href="page_about_me.html">ABOUT ME</a></li>
            <li><a href="page_about_us.html">ABOUT US</a></li>
            <li><a href="page_contact_1.html">CONTACT US 1</a></li>
            <li><a href="page_contact_2.html">CONTACT US 2</a></li>
          </ul> -->
        </li>
        <li>
          <a href="#">
            BACK TO SITE
          </a>
          <ul>
            <li>
              <a href="#">
                FULLWIDTH
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="blog_fullwidth_1.html">STYLE 1</a></li>
                <li><a href="blog_fullwidth_2.html">STYLE 2</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                LEFT SIDEBAR
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="blog_left_sidebar_1.html">STYLE 1</a></li>
                <li><a href="blog_left_sidebar_2.html">STYLE 2</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                RIGHT SIDEBAR
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="blog_right_sidebar_1.html">STYLE 1</a></li>
                <li><a href="blog_right_sidebar_2.html">STYLE 2</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                SINGLE
                <span class="arrow-right icon1-chevron-right"></span>
              </a>
              <ul>
                <li><a href="blog_single_fullwidth.html">FULLWIDTH</a></li>
                <li><a href="blog_single_left_sidebar.html">LEFT SIDEBAR</a></li>
                <li><a href="blog_single_right_sidebar.html">RIGHT SIDEBAR</a></li>
              </ul>
            </li>
          </ul>
        </li>
      <!--   <li>
          <a href="page_shortcodes.html">SHORTCODES</a>
        </li> -->
      </ul>
    </nav>

    <a href="#" class="mobile-navigation icon3-list2"></a>

  </header>
  <!-- End Header -->

     @yield('content')


    <!-- Begin Footer -->
    <footer>

        <p class="copyrights">© ONORIGIN | ALL RIGHTS RESERVED</p>

        <div class="social-networks clearfix">
            <a href="#">
                <span>FACEBOOK</span>
                <i class="facebook-icon icon1-facebook"></i>
            </a>
            <a href="#">
                <span>TWITTER</span>
                <i class="twitter-icon icon1-twitter"></i>
            </a>
            <a href="#">
                <span>PINTEREST</span>
                <i class="pinterest-icon icon3-pinterest"></i>
            </a>
            <a href="#">
                <span>INSTAGRAM</span>
                <i class="instagram-icon icon3-instagram"></i>
            </a>
        </div>

    </footer>
    <!-- End Footer -->


    <!-- Begin JavaScript -->
    <script type="text/javascript" src="{{ asset('uno/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/modernizr-respond.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/retina.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/scrollTo-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/easing.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/appear.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/imagesloaded.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/jflickrfeed.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/flexslider/flexslider.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/queryloader2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/gmap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/nicescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/Magnific-Popup/magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/mb.YTPlayer/inc/mb.YTPlayer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/mousewheel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/lazyload.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('uno/js/scripts.js') }}"></script>
    <!-- End JavaScript -->


</body>
<!-- End Body -->

</html>
