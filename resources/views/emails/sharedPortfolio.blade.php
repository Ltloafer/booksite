@component('mail::message')
# Shared portfolio

Golden Ticket have shared a portfolio with you.

@component('mail::button', ['url' => $url])
View portfolio
@endcomponent

Thanks, <br>
Lex

@endcomponent
