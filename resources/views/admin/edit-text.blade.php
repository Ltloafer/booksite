<!-- Modal -->
<div class="modal fade" id="edit-property-text" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit description</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/property-text/{{$property->id}}" enctype="multipart/form-data">
                {{ method_field('PUT') }} 
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                      <div class="input-group mb-3">
                       <textarea rows="4" cols=100% name="description">{{ $property->description }}</textarea>

                   </div>
               </div>
           </div>

           <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
</div>
</div>


