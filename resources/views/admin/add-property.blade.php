<!-- Modal -->
<div class="modal fade" id="add-property" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add property</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/properties" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-10">
                                <!-- <div class="form-group">
                                    <label for="phone" class="col-md-6 control-label">Address</label>

                                    <div class="col-md-12">
                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>
                                    </div>
                                </div> -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="house_no" class="col-md-6 control-label">House name or number</label>
                                            <div class="col-md-8">
                                                <input id="house_no" type="text" class="form-control" name="house_no" value="{{ old('house_no') }}" autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="street_name" class="col-md-6 control-label">Street name</label>
                                            <div class="col-md-12">
                                                <input id="street_name" type="text" class="form-control" name="street_name" value="{{ old('street_name') }}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="postcode" class="col-md-4 control-label">Town</label>
                                            <div class="col-md-12">
                                                <input id="postcode" type="text" class="form-control" name="town" value="{{ old('town') }}" required>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="postcode" class="col-md-4 control-label">Postcode</label>
                                            <div class="col-md-8">
                                                <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <label for="exampleFormControlSelect1">Contact status</label>

                                        <select class="form-control" id="exampleFormControlSelect1" name="contact_status">
                                            <option value="direct">Direct</option>
                                            <option value="split_comm">Split comm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
