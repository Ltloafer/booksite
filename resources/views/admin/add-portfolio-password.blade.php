<!-- Modal -->
<div class="modal fade" id="add-portfolio-password" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Set portfolio password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/set-portfolio-password" enctype="multipart/form-data">
                <input type="hidden" name="portfolioId" value="{{ $portfolio->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-10">
                          
                                <div class="row">
                                    <div class="col-md-12">

                                        <p>This will set a new password for this portfolio.</p>

                                        <div class="form-group">
                                            <label for="house_no" class="col-md-6 control-label">Password</label>
                                            <div class="col-md-12">
                                                <input id="password" type="password" class="form-control" name="custom_password" value="{{ old('custom_password') }}" required autofocus>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

               


              
