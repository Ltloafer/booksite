@extends('bookSite::admin-layout')
@section('content')

<admin-property :images="{{ $property->images }}" :propertyid= "{{ $property->id }}" inline-template>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <span>
                        <a href="/book-site/admin/properties">PROPERTIES</a><span style="color: #6c757d"> /  {{ $property->displayFullAddress() }}</span>
                    </span>

                  <!--   <a href="/book-site/admin/v2/preview-property/{{ $property->slugged_name }}" class="btn btn-sm btn-outline-primary add-btn float-right ml-3">Preview V2</a>
                     -->
                    <a href="/book-site/preview/property/{{ $property->slugged_name }}" class="btn btn-sm btn-outline-primary add-btn float-right ml-3">Preview</a>

                    <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#edit-property-address"> Edit details</button>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">Description
                                    <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#edit-property-text"> Edit description</button>
                                </div>
                                <div class="card-body">
                                    <p>{{ $property->description }} </p>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">Map
                                            <!--       <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#edit-property-text"> Edit</button> -->
                                        </div>
                                        <div class="card-body">
                                            <property :property=" {{ $property }}" inline-template>
                                                <gmap-map
                                                :center="{lat: latitude, lng: longitude}"
                                                :zoom="17"
                                                map-type-id="roadmap"
                                                :options="{
                                                zoomControl: false,
                                                mapTypeControl: false,
                                                scaleControl: true,
                                                streetViewControl: false,
                                                rotateControl: false,
                                                fullscreenControl: false,
                                                disableDefaultUi: false,
                                                scrollWheelZoom: false,
                                                marker: true
                                            }">

                                            <gmap-marker
                                            :key="index"
                                            v-for="(m, index) in markers"
                                            :position="m.position"
                                            :clickable="false"
                                            :draggable="false"
                                            @click="center=m.position"
                                            />
                                        </gmap-map>
                                    </property>
                                </div>
                            </div>

                            <div class="card mt-5">
                                <div class="card-header">Images
                                    <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#add-image">Add image</button>
                                </div>

                                <div class="card-body">
                                    <draggable
                                    :list="adminImages"
                                    :disabled="!enabled"
                                    class="row wrap fill-height align-center sortable-list"
                                    ghost-class="ghost"
                                    :move="checkMove"
                                    @update="onUpdate"
                                    @start="dragging = true"
                                    @end="dragging = false"
                                    >
                                    <div class="col-sm-2"
                                    v-for="image in adminImages"
                                    :key="image.id"
                                    >
                                    <div class="row mt-3">
                                        <div class="col-sm-12">
                                            <div class="dropdown">
                                                <a href="#" id="imageDropdown" data-toggle="dropdown">
                                                    <img :src=" '/storage/images/' + image.property_id + '/' + image.filename" height=150px width=150px alt="..." class="img-thumbnail img-fluid dropdown-toggle" v-bind:class="{ active: property.title_image_id == image.id }" data-toggle="tooltip" data-placement="right" :title="image.filename">
                                                </a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="imageDropdown" style="font-size: inherit">
                                                    <a hef="/" class="dropdown-item" v-on:click="setHeaderImage(image)">Set as header image</a>
                                                    <li role="presentation" class="divider"></li>
                                                    <a href="/" class="dropdown-item" v-on:click.prevent="removeImage(image)">Remove</a>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </draggable>
                        </div>

                        <hr>

                        <div class="card-body">
                            @foreach($imageFiles as $imageFile)
                            <div class="row">
                                <div class="col-sm-8">
                                    {{ $imageFile->getPathname() }}
                                </div>

                                <div class="col-sm-4">
                                    {{ $imageFile->getSize() }}K
                                </div>
                            </div>
                            @endforeach
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('bookSite::admin.edit-text')
    @include('bookSite::admin.add-image')
    @include('bookSite::admin.edit-property-address')
    @include('bookSite::admin.delete-property')
    <!-- </div> -->
    <button class="btn btn-sm btn-outline-danger float-right" data-toggle="modal" data-target="#delete-property"> Delete property</button>

</div>
</div>
</div>
</div>
</div>
</admin-property>

@endsection
