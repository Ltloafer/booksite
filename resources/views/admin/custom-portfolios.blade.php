@extends('bookSite::admin-layout')
@section('content')

<custom-portfolios :portfolios = "{{ $portfolios }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Custom portfolios
                            <button class="btn btn-sm btn-outline-secondary add-btn ml-2 float-right" data-toggle="modal" data-target="#add-custom-portfolio"> Add portfolio +</button>
                            <input class="search float-right mr-4" type="text" v-model="searchQuery" placeholder="Filter portfolios...">
                        </div>

                        <div class="card-body">

                          <displaydata :data="portfolios" :columns="['portfolio']" :filter-key="searchQuery" inline-template>

                            <div>
                                <table class="table m-b-none">
                                    <thead>
                                        <th v-for="key in columns"
                                        @click="sortBy(key)"
                                        :class="{ active: sortKey == key }">
                                        @{{ key | capitalize }}
                                        <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                        </span>
                                    </th>
                                </thead>

                                <tbody>
                                    <tr v-for="portfolio in filteredData">

                                        <td>@{{ portfolio.name }}</td>
                                        <td>
                                            <a :href=" '/book-site/admin/custom-portfolios/' + portfolio.slugged_name" class="btn btn-outline-secondary btn-sm">View</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </displaydata>
                </div>
            </div>
        </div>

    </div>

    @include('bookSite::admin.add-custom-portfolio')

</div>

</div>
</custom-portfolios>

@endsection
