@extends('bookSite::admin-layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Members
                    <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#add-user"> Add a member +</button>
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Member</th>
                            <th></th>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <!-- <a href="/book-site/admin/users/{{ $user->id }}/edit"> -->
                                    {{ $user->fullname() }}
                                <!-- </a> -->

                                    <br><small>{{ $user->email}}</small>

                                    @if ($user->isAdmin()) <small> | Admin</small>
                                    @endif
                                </td>

                                <td>
                                    <!-- <a href="/book-site/admin/users/{{ $user->id }}/edit" class="btn btn-outline-secondary btn-sm mr-2">Edit member</a> -->

                                    @if ($user->portfolio)
                                    <a href="/book-site/admin/portfolios/{{ $user->portfolio->slugged_name}}" class="btn btn-outline-secondary btn-sm">View portfolio</a>
                                    @else

                                    <form class="form-horizontal" method="POST" action="/book-site/admin/portfolios" enctype="multipart/form-data">
                                        <input type="hidden" name="userId" value="{{ $user->id}}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-outline-primary btn-sm">Create</button>
                                    </form>
                                    @endif

                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('bookSite::admin.register-user')
</div>

@endsection
