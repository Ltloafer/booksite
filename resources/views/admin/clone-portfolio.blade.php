<!-- Modal -->
<div class="modal fade" id="clone-portfolio" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Clone portfolio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/clone-portfolio" enctype="multipart/form-data">
                <input type="hidden" name="portfolioId" value="{{ $portfolio->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-10">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="street_name" class="col-md-6 control-label">Name</label>
                                            <div class="col-md-12">
                                                <input id="clone_name" type="text" class="form-control" name="clone_name" value="{{ old('clone_name') }}" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="street_name" class="col-md-6 control-label">Password</label>
                                            <div class="col-md-12">
                                                <input id="clone_password" type="text" class="form-control" name="custom_password" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>





