@extends('bookSite::admin-layout')
@section('content')

<admin-properties :properties = "{{ $properties }}" inline-template>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Admininstration
                        <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#add-property"> Add a property +</button>
                        <input class="search float-right mr-4" type="text" v-model="searchQuery" placeholder="Filter properties...">
                    </div>

                    <div class="card-body">

                        <displaydata :data="properties" :columns="['property']" :filter-key="searchQuery" inline-template>

                            <div>
                                <table class="table m-b-none">
                                    <thead>
                                        <th v-for="key in columns"
                                        @click="sortBy(key)"
                                        :class="{ active: sortKey == key }">
                                        @{{ key | capitalize }}
                                        <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                        </span>
                                    </th>
                                    </thead>

                                        <tbody>
                                            <tr v-for="property in filteredData" class='clickable-row' v-on:click="">
                                                <td>
                                                    @{{ property.fullAddress }}

                                                    <span v-if="property.direct"><br><small>Direct</small></span>

                                                </td>
                                                <td><a :href=" '/book-site/admin/properties/' + property.slugged_name" class="btn btn-outline-secondary btn-sm">View</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </displaydata>
                        </div>
                    </div>
                </div>
            </div>


                  <!--   <tbody>
                        @foreach($properties as $property)
                        <tr>
                            <td>{{ $property->displayFullAddress() }}</td>
                            <td><a href="/book-site/admin/properties/{{ $property->slugged_name }}" class="btn btn-outline-secondary btn-sm">View</a></td>
                        </tr>
                        @endforeach
                    </tbody> -->


                    @include('bookSite::admin.add-property')

                </div>
            </admin-properties>

            @endsection
