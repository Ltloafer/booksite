<!-- Modal -->
<div class="modal fade" id="delete-property" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete property</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/properties/{{ $property->id }}" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{ method_field('DELETE') }} 

                <div class="modal-body">
                    <div class="container-fluid">

                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>


