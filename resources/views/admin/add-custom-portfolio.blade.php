<!-- Modal -->
<div class="modal fade" id="add-custom-portfolio" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add portfolio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/custom-portfolios" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-10">
                          
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="house_no" class="col-md-6 control-label">Name</label>
                                            <div class="col-md-12">
                                                <input id="custom_name" type="text" class="form-control" name="custom_name" autocomplete="off" autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="street_name" class="col-md-6 control-label">Password</label>
                                            <div class="col-md-12">
                                                <input id="custom_password" type="password" class="form-control" name="custom_password" required>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


