@extends('bookSite::admin-layout')
@section('content')

<portfolio-properties  :portfolioid="{{ $portfolio->id }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Portfolios / {{ $portfolio->name }}
                            <a href="/book-site/admin/portfolios/{{ $portfolio->slugged_name }}/edit" class="btn btn-sm btn-outline-secondary add-btn float-right">Add / remove properties</a>
                        </div>

                        <div class="card-body">
                            <div class="row mt-0">
                                <div class="col-sm-12">

                                 <draggable
                                 :list="properties"
                                 :disabled="!enabled"
                                 class="list-group"
                                 ghost-class="ghost"
                                 :move="checkMove"
                                 @update="onUpdate"
                                 @start="dragging = true"
                                 @end="dragging = false"
                                 >
                                 <div
                                 class="list-group-item"
                                 v-for="property in properties"
                                 :key="property.id"
                                 >

                                 @{{ property.fullAddress }}
                                 <span v-if="property.hasCustomText">
                                    <button class="btn btn-sm btn-outline-primary add-btn float-right" v-on:click.prevent="editCustomText(property)">@{{ property.customText }}</button>
                                </span>

                                <span v-else>
                                    <button class="btn btn-sm btn-outline-success add-btn float-right" v-on:click.prevent="editCustomText(property)">Add custom text</button>
                                </span>

                                <span v-if="property.periodText">
                                    <button class="btn btn-sm btn-outline-primary add-btn float-right mr-5" v-on:click.prevent="editDetails(property)">@{{ property.periodText }}</button>
                                </span>

                                <span v-else>
                                    <button class="btn btn-sm btn-outline-success add-btn float-right mr-5" v-on:click.prevent="editDetails(property)">Add period</button>
                                </span>

                                    <span v-if="property.price">
                                    <button class="btn btn-sm btn-outline-primary add-btn float-right mr-5" v-on:click.prevent="editDetails(property)">@{{ property.price }}</button>
                                </span>

                                <span v-else>
                                    <button class="btn btn-sm btn-outline-success add-btn float-right mr-5" v-on:click.prevent="editDetails(property)">Add price</button>
                                </span>

                            </div>
                        </draggable>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="edit-custom-text" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit text</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" :action=" '/book-site/admin/custom-property-texts/' + property.id" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <input type="hidden" name="portfolioId" value="{{ $portfolio->id }}">

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="input-group mb-3">
                            <textarea rows="6" cols=100% name="text">@{{ property.customText }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="edit-details" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" :action=" '/book-site/admin/portfolio/' + portfolioid + '/properties/' + property.id" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <input type="hidden" name="portfolioId" value="{{ $portfolio->id }}">

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">
                            <input id="price" type="text" class="form-control" name="price" :value="property.price" placeholder="Add price" autocomplete="off">
                        </div>

                            <div class="form-group">
                            <input id="price" type="text" class="form-control" name="period" :value="property.periodText" placeholder="Add period text" autocomplete="off">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
</portfolio-properties>


@endsection
