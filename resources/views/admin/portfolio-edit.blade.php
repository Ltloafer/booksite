@extends('bookSite::admin-layout')
@section('content')

<portfolio-properties :allproperties = "{{ $properties }}" :portfolio = "{{ $portfolio }}" :portfolioproperties= "{{ $portfolioProperties }}" :portfolioid="{{ $portfolio->id }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Portfolios  / {{ $portfolio->name }}
                            <a href="/book-site/admin/custom-portfolios/{{ $portfolio->slugged_name }}" class="btn btn-sm btn-outline-secondary add-btn float-right">Done</a>
                        </div>

                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm-6">

                                    <input class="search float-left mb-2" type="text" v-model="searchProperties" placeholder="Filter properties...">

                                    <displaydata :data="propertiesNotInPortfolio" :portfolio=" {{ $portfolio }}" :columns="['property']" :filter-key="searchProperties" inline-template>

                                        <div>
                                            <table class="table m-b-none">
                                                <thead>
                                                    <th v-for="key in columns"
                                                    @click="sortBy(key)"
                                                    :class="{ active: sortKey == key }">
                                                    @{{ key | capitalize }}
                                                    <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                                    </span>
                                                </th>
                                            </thead>

                                            <tbody>

                                                <template v-for="property in filteredData">
                                                    <tr v-if="property.direct"> 

                                                        <td>@{{ property.displayFullAddress }}
                                                            <br>
                                                            <small><strong>Direct</strong></small>
                                                        </td>

                                                        <td>
                                                            <img :src="property.imageFilename" height=150px width=150px alt="..." class="img-thumbnail img-fluid" style="border: 1px solid #7e531e">
                                                        </td>

                                                        <td>
                                                            <button class="btn btn-outline-primary btn-sm" v-on:click.prevent="addPropertyToPortfolio(property, portfolio)">Add</button>
                                                        </td>         
                                                    </tr>

                                                    <tr v-else> 

                                                        <td>@{{ property.displayFullAddress }}</td>

                                                        <td>
                                                            <img :src="property.imageFilename" height=150px width=150px alt="..." class="img-thumbnail img-fluid">
                                                        </td>

                                                        <td>
                                                            <button class="btn btn-outline-primary btn-sm" v-on:click.prevent="addPropertyToPortfolio(property, portfolio)">Add</button>
                                                        </td>         

                                                    </tr>
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                </displaydata>
                            </div>     

                            <div class="col-sm-6">
                                <input class="search float-left mb-2" type="text" v-model="searchProperties" placeholder="Filter properties..." style="opacity: 0">
                                <table class="table m-b-none">
                                    <thead>
                                        <th>Portfolio properties</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <tr v-for="property in properties">
                                            <td>@{{ property.fullAddress }}</td>
                                          
                                            <td>
                                                <button class="btn btn-outline-danger btn-sm" v-on:click.prevent="removePropertyFromPortfolio(property, portfolio)">Remove</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</portfolio-properties>

@endsection
