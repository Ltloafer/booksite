@extends('bookSite::admin-layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>
                        IMAGES
                    </span>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            @foreach($properties as $property)

                            <p><strong>{{ $property->displayFullAddress() }} - {{ $property->id }}</strong></p>

                            @foreach(['mobile', 'medium', 'wide', 'tiny'] as $folder)

                            {{ $folder }} <br>

                            @foreach($property->imageFilesFromDirectory($folder) as $imageFile)
                            <div class="row">
                                <div class="col-sm-8">
                                    {{ $imageFile->getFilename() }}
                                </div>

                                <div class="col-sm-4">
                                    {{ $imageFile->getSize() }}
                                </div>
                            </div>
                            @endforeach

                            <div class="row">
                                <div class="col-sm-8">
                                    Total File Size 
                                </div>

                                <div class="col-sm-4">
                                    {{ $property->totalFilesSizeFromDirectory($folder) }}M
                                </div>
                            </div>

                            <p></p>

                            @endforeach

                         
                            default<br>
                            @foreach($property->imageFilesFromDirectory() as $imageFile)
                            <div class="row">
                                <div class="col-sm-8">
                                    {{ $imageFile->getFilename() }}
                                </div>

                                <div class="col-sm-4">
                                    {{ $imageFile->getSize() }}
                                </div>
                            </div>
                            @endforeach

                            <div class="row">
                                <div class="col-sm-8">
                                    Total File Size 
                                </div>

                                <div class="col-sm-4">
                                    {{ $property->totalFilesSizeFromDirectory() }}M
                                </div>
                            </div>

                            <hr>

                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
